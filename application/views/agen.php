<section id="faqs" class="faq-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="section-title mb-45">
                        <h3 class="title">Daftar Agen Kutus Kutus</h3>
                    </div>
                </div>
            </div>
            <div class="faq-box">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="faq-accordion">
                            <div class="accrodion-grp" data-grp-name="faq-accrodion">
                                <div class="accrodion active  animated wow fadeInRight" data-wow-duration="1500ms"
                                    data-wow-delay="0ms">
                                    <div class="accrodion-inner">
                                        <div class="accrodion-title">
                                            <h4>Kadek Bondan Noviada (Gianyar)</h4>
                                        </div>
                                        <div class="accrodion-content">
                                            <div class="inner">
                                                <p>Jalan Ratna No 68 G, Kecamatan Sawan, Desa Bungkulan</p><br>
                                                <p>Harga RP. 230.000</p>
                                                <br>
                                                <a target="blank" href="https://goo.gl/maps/yw8U92kcvQ4ER7PEA">Google Maps</a>
                                            </div><!-- /.inner -->
                                        </div>
                                    </div><!-- /.accrodion-inner -->
                                </div>
                            </div>
                            <div class="accrodion-grp" data-grp-name="faq-accrodion">
                                <div class="accrodion  animated wow fadeInRight" data-wow-duration="1500ms"
                                    data-wow-delay="0ms">
                                    <div class="accrodion-inner">
                                        <div class="accrodion-title">
                                            <h4>Putu Yoga Apriadi (Gianyar)</h4>
                                        </div>
                                        <div class="accrodion-content">
                                            <div class="inner">
                                                <p>Jalan Ratna No 68 G, Kecamatan Sawan, Desa Bungkulan</p><br>
                                                <p>Harga RP. 230.000</p>
                                                <br>
                                                <a target="blank" href="https://goo.gl/maps/yw8U92kcvQ4ER7PEA">Google Maps</a>
                                            </div><!-- /.inner -->
                                        </div>
                                    </div><!-- /.accrodion-inner -->
                                </div>
                            </div>
                            <div class="accrodion-grp" data-grp-name="faq-accrodion">
                                <div class="accrodion  animated wow fadeInRight" data-wow-duration="1500ms"
                                    data-wow-delay="0ms">
                                    <div class="accrodion-inner">
                                        <div class="accrodion-title">
                                            <h4>Nyoman Yodie Aryantika (Gianyar)</h4>
                                        </div>
                                        <div class="accrodion-content">
                                            <div class="inner">
                                                <p>Jalan Ratna No 68 G, Kecamatan Sawan, Desa Bungkulan</p><br>
                                                <p>Harga RP. 230.000</p>
                                                <br>
                                                <a target="blank" href="https://goo.gl/maps/yw8U92kcvQ4ER7PEA">Google Maps</a>
                                            </div><!-- /.inner -->
                                        </div>
                                    </div><!-- /.accrodion-inner -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="faq-play-extra">
                <img src="assets/images/faq-play-thumb.jpg">
            </div>
        </div>
        <div class="faq-shape"></div>
    </section>