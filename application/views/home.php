    <!--====== HERO PART START ======-->

    <section class="hero-area bg_cover" style="background-image: url('assets/images/hero-bg.jpg');">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="hero-content">
                        <h1 class="title"><span>Kutus Kutus</span> Support System</h1>
                        <ul class="nav">
                            <li><a class="main-btn" href="<?php echo base_url();?>agen">Temukan Agen <i class="fas fa-angle-right"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="hero-thumb">
                        <img src="assets/images/products/minyak-balur.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--====== HERO PART ENDS ======-->

    <!--====== FEATURES PART START ======-->

    <section id="intro" class="features-area">
        <div class="container custom-container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="features-box">
                        <div class="features-title">
                            <h3 class="title">Sejarah Minyak Kutus Kutus</h3>
                        </div>
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="features-item mt-30">
                                    <i class="fal fa-comments"></i>
                                    <h4 class="title">1999 - 2000</h4>
                                    <p>Make. Lights us. Is life all make midst a moveth forth under may Cattle moved
                                        without seas first air beast place after.</p>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="features-item mt-30">
                                    <i class="fal fa-users-crown"></i>
                                    <h4 class="title">2000 - 2018</h4>
                                    <p>Make. Lights us. Is life all make midst a moveth forth under may Cattle moved
                                        without seas first air beast place after.</p>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="features-item mt-30">
                                    <i class="fal fa-user-lock"></i>
                                    <h4 class="title">2018 - Sekarang</h4>
                                    <p>Make. Lights us. Is life all make midst a moveth forth under may Cattle moved
                                        without seas first air beast place after.</p>
                                </div>
                            </div>
                        </div>
                        <div class="features-play">
                            <a class="video-popup" href="https://www.youtube.com/watch?time_continue=74&v=S2IBwp6I-Q4"><i
                                    class="fas fa-play"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--====== FEATURES PART ENDS ======-->

    <!--====== CAPTURE PART START ======-->

    <section class="capture-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="section-title section-title-bondan">
                        <h2 class="title">Agen Tersebar di Berbagai Daerah di Indonesia</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="col-12">
                <div class="capture-active">
                    <div class="capture-item mt-30">
                        <div class="capture-thumb">
                            <img src="assets/images/capture-1.jpg" alt="">
                        </div>
                        <div class="capture-content">
                            <i class="fal fa-user-headset"></i>
                            <h4 class="title">Make as many video calls you like</h4>
                            <p>Make lights us is life all make midst a moveth forth under may cattle moved without seas
                                first air beast place after.</p>
                        </div>
                    </div>
                    <div class="capture-item mt-30">
                        <div class="capture-thumb">
                            <img src="assets/images/capture-2.jpg" alt="">
                        </div>
                        <div class="capture-content">
                            <i class="fal fa-user-headset"></i>
                            <h4 class="title">Make as many video calls you like</h4>
                            <p>Make lights us is life all make midst a moveth forth under may cattle moved without seas
                                first air beast place after.</p>
                        </div>
                    </div>
                    <div class="capture-item mt-30">
                        <div class="capture-thumb">
                            <img src="assets/images/capture-3.jpg" alt="">
                        </div>
                        <div class="capture-content">
                            <i class="fal fa-user-headset"></i>
                            <h4 class="title">Make as many video calls you like</h4>
                            <p>Make lights us is life all make midst a moveth forth under may cattle moved without seas
                                first air beast place after.</p>
                        </div>
                    </div>
                    <div class="capture-item mt-30">
                        <div class="capture-thumb">
                            <img src="assets/images/capture-2.jpg" alt="">
                        </div>
                        <div class="capture-content">
                            <i class="fal fa-users"></i>
                            <h4 class="title">Create unlimited private groupse</h4>
                            <p>Make lights us is life all make midst a moveth forth under may cattle moved without seas
                                first air beast place after.</p>
                        </div>
                    </div>
                    <div class="capture-item mt-30">
                        <div class="capture-thumb">
                            <img src="assets/images/capture-3.jpg" alt="">
                        </div>
                        <div class="capture-content">
                            <i class="fal fa-cloud-download"></i>
                            <h4 class="title">Get unlimited data cloud storage</h4>
                            <p>Make lights us is life all make midst a moveth forth under may cattle moved without seas
                                first air beast place after.</p>
                        </div>
                    </div>
                    <div class="capture-item mt-30">
                        <div class="capture-thumb">
                            <img src="assets/images/capture-4.jpg" alt="">
                        </div>
                        <div class="capture-content">
                            <i class="fal fa-images"></i>
                            <h4 class="title">Send photos, videos and other files</h4>
                            <p>Make lights us is life all make midst a moveth forth under may cattle moved without seas
                                first air beast place after.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--====== CAPTURE PART ENDS ======-->

    <!--====== FAQ PART START ======-->

    <section id="faqs" class="faq-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="section-title mb-45">
                        <h3 class="title">Frequently Asked Question</h3>
                    </div>
                </div>
            </div>
            <div class="faq-box">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="faq-accordion">
                            <div class="accrodion-grp" data-grp-name="faq-accrodion">
                                <div class="accrodion active  animated wow fadeInRight" data-wow-duration="1500ms"
                                    data-wow-delay="0ms">
                                    <div class="accrodion-inner">
                                        <div class="accrodion-title">
                                            <h4>Apa Manfaat Utama Minyak Kutus Kutus?</h4>
                                        </div>
                                        <div class="accrodion-content">
                                            <div class="inner">
                                                <p>Had light saw so waters together appear form you're can't above forth
                                                    air it may had fly image a is she'd male brought stars thing tree
                                                    over fly that greater he blessed cattle was beast they're doesn't
                                                    his.</p>
                                            </div><!-- /.inner -->
                                        </div>
                                    </div><!-- /.accrodion-inner -->
                                </div>
                                <div class="accrodion   animated wow fadeInRight" data-wow-duration="1500ms"
                                    data-wow-delay="300ms">
                                    <div class="accrodion-inner">
                                        <div class="accrodion-title">
                                            <h4>Berapa Harga Minyak Kutus Kutus ?</h4>
                                        </div>
                                        <div class="accrodion-content">
                                            <div class="inner">
                                                <p>Had light saw so waters together appear form you're can't above forth
                                                    air it may had fly image a is she'd male brought stars thing tree
                                                    over fly that greater he blessed cattle was beast they're doesn't
                                                    his.</p>
                                            </div><!-- /.inner -->
                                        </div>
                                    </div><!-- /.accrodion-inner -->
                                </div>
                                <div class="accrodion  animated wow fadeInRight" data-wow-duration="1500ms"
                                    data-wow-delay="600ms">
                                    <div class="accrodion-inner">
                                        <div class="accrodion-title">
                                            <h4>Dimana saya bisa mendapatkan minyak Kutus Kutus?</h4>
                                        </div>
                                        <div class="accrodion-content">
                                            <div class="inner">
                                                <p>Had light saw so waters together appear form you're can't above forth
                                                    air it may had fly image a is she'd male brought stars thing tree
                                                    over fly that greater he blessed cattle was beast they're doesn't
                                                    his.</p>
                                            </div><!-- /.inner -->
                                        </div>
                                    </div><!-- /.accrodion-inner -->
                                </div>
                                <div class="accrodion  animated wow fadeInRight" data-wow-duration="1500ms"
                                    data-wow-delay="600ms">
                                    <div class="accrodion-inner">
                                        <div class="accrodion-title">
                                            <h4>Bagaimana cara penggunaan minyak Kutus Kutus?</h4>
                                        </div>
                                        <div class="accrodion-content">
                                            <div class="inner">
                                                <p>Had light saw so waters together appear form you're can't above forth
                                                    air it may had fly image a is she'd male brought stars thing tree
                                                    over fly that greater he blessed cattle was beast they're doesn't
                                                    his.</p>
                                            </div><!-- /.inner -->
                                        </div>
                                    </div><!-- /.accrodion-inner -->
                                </div>
                                <div class="accrodion  animated wow fadeInRight" data-wow-duration="1500ms"
                                    data-wow-delay="600ms">
                                    <div class="accrodion-inner">
                                        <div class="accrodion-title">
                                            <h4>Bagaimana cara menjadi agen minyak Kutus Kutus?</h4>
                                        </div>
                                        <div class="accrodion-content">
                                            <div class="inner">
                                                <p>Had light saw so waters together appear form you're can't above forth
                                                    air it may had fly image a is she'd male brought stars thing tree
                                                    over fly that greater he blessed cattle was beast they're doesn't
                                                    his.</p>
                                            </div><!-- /.inner -->
                                        </div>
                                    </div><!-- /.accrodion-inner -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="faq-play-extra">
                <img src="assets/images/faq-play-thumb.jpg">
            </div>
        </div>
        <div class="faq-shape"></div>
    </section>

    <!--====== FAQ PART ENDS ======-->

    <!--====== INTRO PART START ======-->

    <section id="features" class="intro-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-7">
                    <div class="section-title text-center">
                        <h3 class="title">Bahan Alami Minyak Kutus Kutus</h3>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="intro-thumb">
                        <img src="assets/images/intro-thumb.png" alt="">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="intro-content">
                        <div class="intro-item mb-30">
                            <i class="fal fa-user-headset"></i>
                            <h4 class="title">Dari Alam</h4>
                            <p>He saw lesser whales man air. Seasons void fly replenish man divided open fifth own
                                fruitful above catt le face.</p>
                        </div>
                        <div class="intro-item mb-30">
                            <i class="fal fa-users"></i>
                            <h4 class="title">Untuk Alam</h4>
                            <p>Give whales creeping sixth. Blessed itself created dry they're firmament face whose face
                                lesser spirit day dry.</p>
                        </div>
                        <div class="intro-item mb-30">
                            <i class="fal fa-cloud-download"></i>
                            <h4 class="title">Apotik Alam</h4>
                            <p>Waters seasons of place likeness good day let they're evening replenish years of. Over
                                that, so let which subdue.</p>
                        </div>
                        <div class="intro-item">
                            <i class="fal fa-images"></i>
                            <h4 class="title">Alam Segalanya</h4>
                            <p>Stars that is likeness yielding isn't image itself creeping. Cattle isn't isn't life
                                second make male replenish us face.</p>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--====== INTRO PART ENDS ======-->

    <!--====== TESTIMONIAL PART START ======-->

    <section id="feedbacks" class="testimonial-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="section-title">
                        <h3 class="title">Testimoni Kutus Kutus</h3>
                    </div>
                </div>
            </div>
            <div class="row testimonial-active">
                <div class="col-lg-4">
                    <div class="testimonial-item mt-30">
                        <img src="assets/images/testimonial-1.png" alt="">
                        <p>“Sixth behold beginning and dry. Him form cattle. She'd land fruitful Sixth in. After
                            Dominion whales fill likeness i earth. Third male lights after cattle can't.”</p>
                        <h5 class="title">David Keeve</h5>
                        <span>USA</span>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="testimonial-item mt-30">
                        <img src="assets/images/testimonial-2.png" alt="">
                        <p>“Winged likeness have there beast don't fourth lights you fruit fruit. Lesser years place of
                            made lights let, form lesser, i darkness meat, saying Night.”</p>
                        <h5 class="title">Karren Jake</h5>
                        <span>Australia</span>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="testimonial-item mt-30">
                        <img src="assets/images/testimonial-3.png" alt="">
                        <p>“Moveth our in let life. Deep abundantly beast forth all. Also place the to isn't itself be
                            fifth give gathering is years female fifth us gathered.”</p>
                        <h5 class="title">Stephan Hart</h5>
                        <span>Kuwait</span>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="testimonial-item mt-30">
                        <img src="assets/images/testimonial-2.png" alt="">
                        <p>“Winged likeness have there beast don't fourth lights you fruit fruit. Lesser years place of
                            made lights let, form lesser, i darkness meat, saying Night.”</p>
                        <h5 class="title">Karren Jake</h5>
                        <span>Australia</span>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--====== TESTIMONIAL PART ENDS ======-->

    <!--====== DOWNLOAD PART START ======-->

    <div class="download-area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="section-title">
                        <h3 class="title">30k agents and counting onwards.</h3>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="download-btns">
                        <ul class="nav">
                            <li><a class="main-btn" href="<?php echo base_url();?>agen">Temukan Agen <i class="fas fa-angle-right"></i></a></li>
                         
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--====== DOWNLOAD PART ENDS ======-->


    <!--====== NEWS PART START ======-->

    <section id="blog" class="news-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-9">
                    <div class="section-title text-center">
                        <h3 class="title">Read latest news <br>from our blog.</h3>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-4 col-md-7 col-sm-9">
                    <div class="news-item mt-30">
                        <div class="news-thumb">
                            <img src="assets/images/news-1.jpg" alt="">
                        </div>
                        <div class="news-content">
                            <a href="blog-details.html">
                                <h4 class="title">Ekspansi Kutus Kutus Market Eropa!</h4>
                            </a>
                            <span>12 Comments <span class="divider">|</span> Dec 17, 2020</span>
                            <p>Sixth behold beginning and dry. Him form cattle. She'd land fruitful Sixth in. After
                                Dominion whales fill likeness earth third male lights .</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-7 col-sm-9">
                    <div class="news-item mt-30">
                        <div class="news-thumb">
                            <img src="assets/images/news-2.jpg" alt="">
                        </div>
                        <div class="news-content">
                            <a href="blog-details.html">
                                <h4 class="title">Ekspansi Kutus Kutus Market Eropa!</h4>
                            </a>
                            <span>12 Comments <span class="divider">|</span> Dec 17, 2020</span>
                            <p>Sixth behold beginning and dry. Him form cattle. She'd land fruitful Sixth in. After
                                Dominion whales fill likeness earth third male lights .</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-7 col-sm-9">
                    <div class="news-item mt-30">
                        <div class="news-thumb">
                            <img src="assets/images/news-3.jpg" alt="">
                        </div>
                        <div class="news-content">
                            <a href="blog-details.html">
                                <h4 class="title">Ekspansi Kutus Kutus Market Eropa!</h4>
                            </a>
                            <span>12 Comments <span class="divider">|</span> Dec 17, 2020</span>
                            <p>Sixth behold beginning and dry. Him form cattle. She'd land fruitful Sixth in. After
                                Dominion whales fill likeness earth third male lights .</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="news-btn text-center mt-60">
                        <a class="main-btn main-btn-2" href="blog.html">Visit our blog <i
                                class="fas fa-angle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--====== NEWS PART ENDS ======-->

    <!--====== DOWNLOAD APP PART START ======-->

    <section class="download-app-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <div class="download-app-content">
                        <h3 class="title">Download the app with everything for free.</h3>
                        <ul class="nav">
                            <li><a class="main-btn main-btn-3" href="<?php echo base_url();?>agen">Temukan Agen <i
                                        class="fas fa-angle-right"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="download-app-case">
            <img src="assets/images/download-app-case.png" alt="">
        </div>
    </section>

    <!--====== DOWNLOAD APP PART ENDS ======-->

   