 <!--====== FOOTER PART START ======-->

 <section class="footer-area bg_cover" style="background-image: url('<?php echo base_url();?>/assets/images/hero-bg.jpg');">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="footer-about mt-30">
                        <a href="index.html#" class="mid-logo-footer"><img style="width:90px;" src="<?php echo base_url();?>/assets/images/logo.png" alt=""></a>
                        <p>Kutus Kutus is an app that helps was created for people who have a fast paced lifestyle and find it
                            hard to converse with their loved ones.</p>
                        <ul>
                            <li><a href="index.html#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="index.html#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="index.html#"><i class="fab fa-linkedin-in"></i></a></li>
                            <li><a href="index.html#"><i class="fab fa-youtube"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6">
                    <div class="footer-list mt-40">
                        <h5 class="title">Explore</h5>
                        <ul>
                            <li><a href="index.html#">About</a></li>
                            <li><a href="index.html#">Our Team</a></li>
                            <li><a href="index.html#">Features</a></li>
                            <li><a href="index.html#">Blog</a></li>
                            <li><a href="index.html#">How It Works</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="footer-list mt-40 pl-55">
                        <h5 class="title">Help</h5>
                        <ul>
                            <li><a href="index.html#">FAQ</a></li>
                            <li><a href="index.html#">Privacy Policy</a></li>
                            <li><a href="index.html#">Support</a></li>
                            <li><a href="index.html#">Terms of Service</a></li>
                            <li><a href="index.html#">Contact</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="footer-list footer-newsleatter mt-40">
                        <h5 class="title">Newsletter</h5>
                        <form action="#">
                            <div class="input-box">
                                <input type="text" placeholder="Email address">
                                <button><i class="fas fa-arrow-right"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-12">
                    <span class="copyright"> <a style="color:white;" href="https://redsystem.id">© REDSYSTEM</a></span>
                </div>
            </div>
        </div>
    </section>

    <!--====== FOOTER PART ENDS ======-->

    <!--====== BACK TO TOP START ======-->

    <a class="back-to-top">
        <i class="fal fa-angle-up"></i>
    </a>

    <!--====== BACK TO TOP ENDS ======-->







    <!--====== jquery js ======-->
    <script src="<?php echo base_url();?>assets/js/vendor/modernizr-3.6.0.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/vendor/jquery-1.12.4.min.js"></script>

    <!--====== Bootstrap js ======-->
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>

    <!--====== scrolling nav js ======-->
    <script src="<?php echo base_url();?>assets/js/jquery.easing.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/scrolling-nav.js"></script>

    <!--====== Slick js ======-->
    <script src="<?php echo base_url();?>assets/js/slick.min.js"></script>

    <!--====== Magnific Popup js ======-->
    <script src="<?php echo base_url();?>assets/js/jquery.magnific-popup.min.js"></script>

    <!--====== Main js ======-->
    <script src="<?php echo base_url();?>assets/js/main.js"></script>

</body>

</html>
